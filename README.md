# Flyway Example Application

###### An example application that shows how to configure the database versioning mechanism - Flyway.
###### On startup, the application creates a database in a text file using the H2 database driver. Then it creates a base structure using Flyway.
###### The entire configuration of the application is located in the application.yml file.
###### The pom.xml file contains the configuration of Flyway plugin for maven.

###### Additionally, the application can be started from the command line (CLI). The command for this is: `mvn compile flyway:migrate`

###### To check the migration result, use the command: `mvn flyway:info`